var WIDTH = window.innerWidth,
	HEIGHT = window.innerHeight,
	ASPECT = WIDTH / HEIGHT,
	UNITSIZE = 250,
	WALLHEIGHT = UNITSIZE,
	MOVESPEED = 100,
    ENEMYSPEED = 100,
	LOOKSPEED = 0.075,
	BULLETMOVESPEED = MOVESPEED * 5,
	NUMAI = 5,
	PROJECTILEDAMAGE = 20,
    BOSSSCALE = 4;

var health = 100;
var healthCube, lastHealthPickup = 0, lastHit = 0;

var renderer;
var scene;
var camera;
var loader;
var projector;
var reproduce = true;
var numModels= 0 ;
var maxNumModels = 0 ;
var movements;
var rayCaster;
var mouse;
var intersectedObject;
var imageContext;
var texture;
var doneLoading = false;
var maxZombies = 0;
var zombieKills = 0 ;
var bossCreated = false;
var bossCharged = false;
var phase2=false;
var image;
var video;
var video2;

mouse = { x: 0, y: 0 };
var BULLETMOVESPEED = 500;
var PROJECTILEDAMAGE = 20;
var HITDAMAGE = 10;
var BOSSHITDAMAGE = 50;
var ZOMBIEHEALTH = 100;
var BOSSHEALTH = 300;

var temp;
//Matriz mapa
var map = [ // 1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,], // 0
           [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,], // 1
           [1, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 1,], // 2
           [1, 0, 3, 2, 0, 0, 2, 0, 0, 2, 0, 0, 1,], // 3
           [1, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 1,], // 4
           [1, 0, 0, 2, 0, 0, 2, 0, 0, 2, 3, 0, 1,], // 5
           [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,], // 6
           [1, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 1,], // 7
           [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,], // 8
           [1, 3, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 1,], // 9
           [1, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1,], // 10
           [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,], // 11
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,], // 16
           ], mapW = map.length, mapH = map[0].length;

window.onload = init;


function getMapSector(v)
{
	var x = Math.floor((v.x + UNITSIZE / 2) / UNITSIZE + mapW/2);
	var z = Math.floor((v.z + UNITSIZE / 2) / UNITSIZE + mapW/2);
	return {x: x, z: z};
}

function checkWallCollision(v)
{
	var c = getMapSector(v);
    return map[c.x][c.z] == 1 ; //Si el vector esta en un muro o en una furgo, en la matriz sera 1 o 2
}

function setMaterial(node, material)
{
    node.material = material;
    if (node.children)
    {
        for (var i = 0; i < node.children.length; i++)
        {
            setMaterial(node.children[i], material);
        }
    }
}


function colisionConBoss(v)
{ 
    var boss = scene.children.filter(function(t) {return t.name.includes('boss')});
    var tempX;
    var tempY;
    var tempZ;
    for(j=0; j<boss.length; j++)
    {
        tempX= boss[j].position.x;
        tempY= boss[j].position.y;
        tempZ= boss[j].position.z;
        var dist = euclideanDistance(v.x, v.y, v.z, tempX, tempY, tempZ);
        if(dist < 65)
        {      
            boss[j].health -= PROJECTILEDAMAGE;        
            if(boss[j].health <= BOSSHEALTH/2)
                setMaterial(boss[j], new THREE.MeshBasicMaterial({color: 0x00FF00}) );
            if(boss[j].health <= 0)
            {
                scene.remove(boss[j]); 
                window.location.href = "victory.html";
            }
            return true;
        }
    }
    return false;  
}


function colisionConZombies1(v)
{ 
    var zombies = scene.children.filter(function(t) {return t.name.includes('zombie')});
    var tempX;
    var tempY;
    var tempZ;
    for(j=0; j<zombies.length; j++){
        tempX= zombies[j].position.x;
        tempY= zombies[j].position.y;
        tempZ= zombies[j].position.z;
        var dist = euclideanDistance(v.x, v.y, v.z, tempX, tempY, tempZ);
        if(dist < 65){                    
            zombies[j].health-= PROJECTILEDAMAGE;         
            if(zombies[j].health <= ZOMBIEHEALTH/2)
                setMaterial(zombies[j], new THREE.MeshBasicMaterial({color: 0xf00000}) );
            if(zombies[j].health <= 0){
                scene.remove(zombies[j]); 
                zombieKills++;
            }
            return true;
        }     
    }
    return false;  
}

function colisionConZombies2(p, b){ // intento de colision recursiva sobre los nodos de los modelos
    
    var ai = scene.children.filter(function(t) {return t.name.includes('zombie')});
    
    for (var j = ai.length-1; j >= 0; j--) {
			var a = ai[j];
			var v = a.geometry.vertices[0];
			var c = a.position;
			var x = Math.abs(v.x), z = Math.abs(v.z);
			//console.log(Math.round(p.x), Math.round(p.z), c.x, c.z, x, z);
			if (p.x < c.x + x && p.x > c.x - x &&
					p.z < c.z + z && p.z > c.z - z &&
					b.owner != a) {
				//bullets.splice(i, 1);
				scene.remove(a);
				//a.health -= PROJECTILEDAMAGE;
				//var color = a.material.color, percent = a.health / 100;
                var percent = 50 ;
				a.material.color.setRGB(
						percent * color.r,
						percent * color.g,
						percent * color.b
				);
				//hit = true;
				return true;
			}
		}
    
    return false;
    
}

function euclideanDistance(x1, y1, z1, x2, y2, z2) 
{
	return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1));
}

function changeName(obj, name)
{
    for ( var i = 0; i < obj.children.length; i ++ ) 
    {
        changeName(obj.children[ i ],name);
    } 
    obj.name = name;
}

function loadDaeModel(path, x, y, z, scale, name) {

    var daeLoader = new THREE.ColladaLoader();
    daeLoader.options.convertUpAxis = true;
    daeLoader.load(path, function(collada) {
    var modelMesh = collada.scene;

    // Prepara y ejecuta la animacion
    modelMesh.traverse( function (child)
    {
        if (child instanceof THREE.SkinnedMesh) 
        {
            var animation = new THREE.Animation(child, child.geometry.animation);
            animation.play();
        }
    });
    // Cambia posicion y escala
    modelMesh.position.x = x;
    modelMesh.position.y = y;
    modelMesh.position.z = z;
    modelMesh.scale.set(scale, scale, scale);
    //Añade caracteristicas especiales a enemigos
    if(path == 'models/zombie.dae' || path =='models/boss.dae')
    {
	   var units = mapW;
       modelMesh.dirPosX = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;
       modelMesh.dirPosZ = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;
       modelMesh.up = new THREE.Vector3(0,1,0);
       modelMesh.lookAt(new THREE.Vector3(modelMesh.dirPosX,modelMesh.height/2,modelMesh.dirPosZ));
       if (path == 'models/zombie.dae')
       {
          modelMesh.health = ZOMBIEHEALTH;
       }
       else 
       {
          modelMesh.health = BOSSHEALTH;
          bossCharged = true;
       }
     }
     //Cambia nombre y añade a escena
     changeName(modelMesh,name);
	 scene.add(modelMesh);
	 numModels++;
	 console.log("Soy numModel: " + numModels);
     //Checkea si es el ultimo modelo cargado para empezar a animar
     if(numModels==maxNumModels)
     {
        image = document.createElement( 'canvas' );
        // Add video
        if(reproduce)
        {
            $('body').append('<video id="video"    style="display:none">' +
                '<source src="videos/wind-fish.mp4">' +
                '</video>');
          
            video = document.getElementById( 'video' );            
            video.addEventListener( "loadedmetadata", function (e) {
                image.width = video.videoWidth;
                image.height = video.videoHeight;
                imageContext = image.getContext( '2d' );
                imageContext.fillStyle = '#000000';
                imageContext.fillRect( 0, 0, image.width, image.height );
                texture = new THREE.Texture( image );
                var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 , side: THREE.DoubleSide} );
                var wall = new THREE.Mesh( new THREE.PlaneGeometry( image.width, image.height, 4, 4 ), material );
                wall.position.z = 1230;
                wall.position.y = 480;
                wall.name="pelicula";
                scene.add(wall);
                video.play();
                doneLoading=true;
            }, false ); 
        }
        else{
            image.width = 854;  
            image.height = 480; 
            imageContext = image.getContext( '2d' );
            imageContext.fillStyle = '#000000';
            imageContext.fillRect( 0, 0, image.width, image.height );
            texture = new THREE.Texture( image );
            var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 , side: THREE.DoubleSide} );
            var wall = new THREE.Mesh( new THREE.PlaneGeometry( image.width, image.height, 4, 4 ), material );
            wall.position.z = -1990;
            wall.position.y = 360;
            scene.add(wall);
        }
          
        
        // Display the HUD: health
	    $('body').append('<div id="hud"><p>Health: <span id="health">100</span></p></div>');
	    // Set up the brief red flash that shows when you get hurt
	    $('body').append('<div id="hurt"></div>');
        $('#hurt').css({width: WIDTH, height: HEIGHT,});
        
        $('#loading-container').remove();
        animate();
      }
    });
  }

function addSceneElements()
{
	var units = mapW;
	//Suelo
	var floor = new THREE.Mesh(
			new THREE.CubeGeometry(units * UNITSIZE, 10, units * UNITSIZE),
			new THREE.MeshLambertMaterial({map: loader.load('textures/asphalt.jpg')})
	);
	scene.add(floor);
 
	//Muros
	var cube = new THREE.CubeGeometry(UNITSIZE, WALLHEIGHT, UNITSIZE);
	var materials = [new THREE.MeshLambertMaterial({map: loader.load('textures/fence.jpg')}),];
	
    //Cuenta objetos a cargar
	for (var i = 0; i < mapW; i++) 
    {
		for (var j = 0, m = map[i].length; j < m; j++) 
        {
			if(map[i][j] != 1 && map[i][j] != 0)
					maxNumModels++;
		}
	}
	console.log("MaxModels: " + maxNumModels);
    
    //Carga los objetos en su posicion
	for (var i = 0; i < mapW; i++) 
    {
		for (var j = 0, m = map[i].length; j < m; j++)
        {
			if (map[i][j]) 
            {
                if (map[i][j] == 1) 
                {
                    var wall = new THREE.Mesh(cube, materials[map[i][j]-1]);
                    wall.position.x = (i - units/2) * UNITSIZE;
                    wall.position.y = WALLHEIGHT/2;
                    wall.position.z = (j - units/2) * UNITSIZE;
                    scene.add(wall);
                }
                else if (map[i][j] == 2)
                {
                    loadDaeModel('models/motorhome.dae',(i - units/2) * UNITSIZE,0.03 * UNITSIZE,(j - units/2) * UNITSIZE,25, "motor" + i + j);
                }
				else if (map[i][j] == 3)
                {
					loadDaeModel('models/zombie.dae',(i - units/2) * UNITSIZE, 0 ,(j - units/2) * UNITSIZE, 0.2, "zombie" + i + j);
                    maxZombies++;
				}
			 }
		}
	}
	
	// Carga el cubo de vida
	healthcube = new THREE.Mesh(
			new THREE.CubeGeometry(30, 30, 30),
			new THREE.MeshBasicMaterial({map: loader.load('textures/health.png')})
	);
	healthcube.position.set(-UNITSIZE-15, 35, -UNITSIZE-15);
	healthcube.name= "healthcube";
	scene.add(healthcube);
    
    //Carga la cupula que hace de cielo
    var skyDomeGeo = new THREE.SphereGeometry(UNITSIZE*15, 25, 25);
    var skyDomeTex = loader.load("textures/space.jpg");
    var materialSky = new THREE.MeshBasicMaterial({ 
        map: skyDomeTex,
    });
    var skyDome = new THREE.Mesh(skyDomeGeo, materialSky);
    skyDome.material.side = THREE.BackSide;
    scene.add(skyDome);
}

function addLights() 
{
    var directionalLight1 = new THREE.DirectionalLight( 0xF7EFBE, 0.7 );
	directionalLight1.position.set( 0.5, 1, 0.5 );
	scene.add( directionalLight1 );
	var directionalLight2 = new THREE.DirectionalLight( 0xF7EFBE, 0.5 );
	directionalLight2.position.set( -0.5, -1, -0.5 );
	scene.add( directionalLight2 );
}

function init() 
{
    // Crea la escena
    scene = new THREE.Scene();
    // Ayuda a proyectar rayos 2D rays (en la pantalla) en rayos 3D  (en la escena)
    projector = new THREE.Projector(); 
    // Carga de texturas loader
    loader = new THREE.TextureLoader();
    
    // Detecta el soporte
    renderer = Detector.webgl ? new THREE.WebGLRenderer( {antialias: true} ): new THREE.CanvasRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    
    // Añade la camara
    camera = new THREE.PerspectiveCamera(60, ASPECT, 1, 10000);
	camera.position.y = UNITSIZE * .2; 
    //Movimientos posibles
	movements = [
		new THREE.Vector3(0, 0, 1),   // Forward
		new THREE.Vector3(1, 0, 1),   // Forward-left
		new THREE.Vector3(1, 0, 0),   // Left
		new THREE.Vector3(1, 0, -1),  // Backward-left
		new THREE.Vector3(0, 0, -1),  // Backward
		new THREE.Vector3(-1, 0, -1), // Backward-right
		new THREE.Vector3(-1, 0, 0),  // Right
		new THREE.Vector3(-1, 0, 1)   // Forward-right
	];
	
    //Crea el objeto de rayos y el raton
	rayCaster = new THREE.Raycaster();
	mouse = new THREE.Vector2();
	intersectedObject = null;
	
    //Añade una escucha de sonida a la camara
	var listener = new THREE.AudioListener();
	camera.add( listener );
    
    // Añade elementos a la escena 
    addSceneElements();    

    // Añade luces
    addLights();
    
    // Enlaza el click al disparo
	$(document).click(function(e) {
		e.preventDefault();
		if (e.which === 1) 
        { // Dispara con el boton izquierdo
			createBullet(); 
		}
	});
    
    //Añade controles en primera persona
    controls = new THREE.FirstPersonControls( camera );
    controls.movementSpeed = MOVESPEED * 5;
    controls.lookSpeed = LOOKSPEED;
    controls.noFly = true;
    controls.lookVertical = false;

    var windowResize = new THREEx.WindowResize( renderer, camera, document.body ); 
    clock = new THREE.Clock( );
    renderer.render( scene, camera );
}


 function animate( ) {
				
		var collisions, i, COLLISION, direction;
		
        //Check si se debe cargar la fase dos
        if(zombieKills == maxZombies && bossCreated == false)
        {
            loadDaeModel('models/boss.dae',(6 - mapW/2) * UNITSIZE, 0 ,(6 - mapW/2) * UNITSIZE, BOSSSCALE, "boss" + 6 + 6);
            bossCreated = true;
            phase2=true;
            doneLoading = false;
                     
            video.pause();
            var wallPeli = scene.getObjectByName("pelicula");
            console.log("NOMBRE DEL MURO: " + wallPeli.name);
            scene.remove(wallPeli);
            
            $('body').append('<video id="video2"   style="display:none">' +
                '<source src="videos/Archangel.mp4">' +
                //'<source src="videos/sintel_trailer-480p.ogv">' +
            '</video>');
            
            video2 = document.getElementById( 'video2' );
            video2.addEventListener( "loadedmetadata", function (e) 
            {
                image.width = video2.videoWidth;
                image.height = video2.videoHeight;
                imageContext = image.getContext( '2d' );
                imageContext.fillStyle = '#000000';
                imageContext.fillRect( 0, 0, image.width, image.height );
                texture = new THREE.Texture( image );

                var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 , side: THREE.DoubleSide} );
                var wall = new THREE.Mesh( new THREE.PlaneGeometry( image.width, image.height, 4, 4 ), material );
                wall.position.z = 1230;
                wall.position.y = 480;
                scene.add(wall);
                video2.play();
                doneLoading=true;
            }, false ); 
            
        } 
		distance = 20;
        var delta = clock.getDelta();
        controls.update( delta );
        var speed = delta * BULLETMOVESPEED;
     
		//Actualiza el cubo de vida
		if (healthcube.material.wireframe == true && Date.now() > lastHealthPickup + 60000)
            healthcube.material.wireframe = false;
     
        //Comprueba colisiones
        for (i=0; i< movements.length ; i++)
        {
            direction = movements[i];
            rayCaster.set( camera.position, direction );
            collisions = rayCaster.intersectObjects( scene.children, true);
            if ( collisions.length > 0 && collisions[0].distance <= distance ) 
            {
                // COLLISION
                console.log('COLISION CON: ' + collisions[0].object.name );	
                //Si no es el cubo
                if (collisions[0].object.name.localeCompare("healthcube") != 0)
                {
                    //Si es un enemigo
                    if ((Date.now() > lastHit + 1000) && ((collisions[0].object.name.includes('zombie')) || (collisions[0].object.name.includes('boss'))))
                    {
                        // Hit
                        $('#hurt').fadeIn(75);
                        health -= collisions[0].object.name.includes('boss') ? BOSSHITDAMAGE : HITDAMAGE;
                        health = health > 0 ? health : 0;
                        $('#health').html(health);
                        $('#hurt').fadeOut(350);
                        // Death
                        if (health <= 0) 
                        {
                            window.location.href = 'gameover.html';
                        }
                        
                        lastHit = Date.now();
                    }
                    // Restaura la posicion
                    controls.update( -delta );
                }
                //Comprobaciones para curar
                console.log(lastHealthPickup);
                if (Date.now() > lastHealthPickup + 60000) {
                    if (collisions[0].object.name.localeCompare("healthcube") == 0 && health != 100){
                        health = Math.min(health + 50, 100);
                        $('#health').html(health);
                        lastHealthPickup = Date.now();
                        healthcube.material.wireframe = true;
                    }
                }
            }
        }
		
     
		// Actualiza balas. Hacia atras para poder ir eliminando objetos
        for (var i = bullets.length-1; i >= 0; i--) 
        {
            var b = bullets[i], p = b.position, d = b.ray.direction;
            //Choca con un muro
            if (checkWallCollision(p)) 
            {
                bullets.splice(i, 1);
                scene.remove(b);
                console.log("Bala eliminada por colision con muro");
                continue;
            }
            //Choca con un zombi
            if( colisionConZombies1(p) ){
                bullets.splice(i, 1);
                scene.remove(b);
                console.log("Bala eliminada por colision con zombie");
                continue;
            }
            //Choca con el boss
            if( colisionConBoss(p) ){
                bullets.splice(i, 1);
                scene.remove(b);
                console.log("Bala eliminada por colision con boss");
                continue;
            }
            //Si llega, no ha chocado
            var hit = false;
            if (!hit) 
            {
                b.translateX(speed * d.x);
                b.translateY(speed * d.y);
                b.translateZ(speed * d.z);
            }
        }
       //Se comprueba que es fase uno o que se ha cargado el boss       
       if(zombieKills != maxZombies || bossCharged==true)
       {  
	       //Movimiento de enemigos
            var enemy = scene.children.filter(function(t) {return t.name.includes('zombie')});
            var boss = scene.children.filter(function(t) {return t.name.includes('boss')});
            enemy.push.apply(enemy, boss);
            //Calculo del movimiento en funcion de delta
            var actualMoveSpeed = delta * ENEMYSPEED;
            if(zombieKills == maxZombies)
            {
                actualMoveSpeed = delta * ENEMYSPEED*4;
            } 
            var units = mapW;
            for (j=0; j<enemy.length;j++)
		    {
                //Almacena las posiciones a las que se dirigen
                var oldX = enemy[j].dirPosX;
                var oldZ = enemy[j].dirPosZ;
                //Con una probabilidad, cambian de direccion
                if(Math.random()>0.999)
                {
                    //Puntos aleatorios entre (-units * UNITSIZE)/2) y (units * UNITSIZE)/2)
                    enemy[j].dirPosX = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;
                    enemy[j].dirPosZ = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;
                } 
                //Calculo las colisiones para definir un nuevo destino si choca
                if(map[Math.round((enemy[j].position.x/ UNITSIZE)+units/2)][Math.round((enemy[j].position.z/ UNITSIZE)+units/2)]==1 ||
                            map[Math.round((enemy[j].position.x/ UNITSIZE)+units/2)][Math.round((enemy[j].position.z/ UNITSIZE)+units/2)]==2)
                {   
                    enemy[j].dirPosX = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;
                    enemy[j].dirPosZ = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;
                }
                //Calculo el modulo del vector con el punto
                var modulo = Math.pow(Math.pow(enemy[j].dirPosX - enemy[j].position.x,2)+Math.pow(enemy[j].dirPosZ - enemy[j].position.z,2),1/2);
                //el enemigo se mueve en funcion del vector unitario hacia el punto si el modulo es superior a 1 (distancia)
                if(modulo>1)
                {
                    enemy[j].position.x += ((enemy[j].dirPosX - enemy[j].position.x)/modulo) * actualMoveSpeed;
                    enemy[j].position.z += ((enemy[j].dirPosZ - enemy[j].position.z)/modulo) * actualMoveSpeed;
                    enemy[j].lookAt(new THREE.Vector3(enemy[j].dirPosX,enemy[j].height/2,enemy[j].dirPosZ)); 
                }
                //si es menor se cambia la direccion, ya habia llegado al viejo (aprox)
                else
                {
                    //Puntos aleatorios entre (-units * UNITSIZE)/2) y (units * UNITSIZE)/2)
                    enemy[j].dirPosX = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;
                    enemy[j].dirPosZ = Math.floor(Math.random() * ((units * UNITSIZE)/2 - (-units * UNITSIZE)/2 + 1)) + (-units * UNITSIZE)/2;  
                }
            }
       }
       // Render the scene
       renderer.render( scene, camera );
       rayCaster.setFromCamera( mouse, camera );

        //Controla la interseccion con objetos Para depuracion
        var intersects = rayCaster.intersectObjects( scene.children, true );
        if ( intersects.length > 0 ) {
            // Sorted by Z (close to the camera)
            if ( intersectedObject != intersects[ 0 ].object && ( intersects[ 0 ].object === undefined == false ) ) 
            {
                intersectedObject = intersects[ 0 ].object;
                console.log( 'New intersected object: ' + intersectedObject.name );

            } else 
            {
                intersectedObject = null;
            }
        }
        
        var videoTemp;               
        if(reproduce && doneLoading)
        {
            if(phase2 == false)
                videoTemp = video
            else
                videoTemp = video2;
            // UPDATE THE SCENE ACCORDING TO THE ELAPSED TIME
            if ( videoTemp.readyState === videoTemp.HAVE_ENOUGH_DATA ) {
                imageContext.drawImage( videoTemp, 0, 0 );
                if ( texture ) texture.needsUpdate = true;
            }
        }
        // Request the browser to execute the animation-rendering loop
        requestAnimationFrame( animate );
 };


var bullets = [];
var sphereMaterial = new THREE.MeshBasicMaterial({color: 0x333333});
var sphereGeo = new THREE.SphereGeometry(2, 6, 6);

function createBullet(obj) 
{
	if (obj === undefined) 
    {
		obj = camera;
	}
	var sphere = new THREE.Mesh(sphereGeo, sphereMaterial);
	sphere.position.set(obj.position.x, obj.position.y * 0.8, obj.position.z);
 
	if (obj instanceof THREE.Camera) 
    {
        var vector = new THREE.Vector3(mouse.x, mouse.y, mouse.z);
		projector.unprojectVector(vector, obj);
		sphere.ray = new THREE.Ray(
				obj.position,
				vector.sub(obj.position).normalize()
		);
	}
	else 
    {
		var vector = camera.position.clone();
		sphere.ray = new THREE.Ray(
				obj.position,
				vector.sub(obj.position).normalize()
		);
	}
	sphere.owner = obj;
	bullets.push(sphere);
	scene.add(sphere);
 
	return sphere;
}
